// $Id$

Installing MyLiveChat in Drupal 6.x:

1. Upload 'mylivechat' directory to '/sites/all/modules/'.
2. Open your Drupal website.
3. Go to "Administer > Site building > Modules" and activate 'MyLiveChat' module.
4. Go to "Administer > Site configuration > MyLiveChat" and follow the instructions.

--- 
For further information, please visit our website:
www.mylivechat.com
